# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.
let
  myPkgs = (import (builtins.fetchTarball {
    name = "nixos-unstable-2020-01-14";
    url = "https://github.com/nixos/nixpkgs/archive/f5140d1b1e92e8fac025b57226b7c26ca3fb6ffc.tar.gz";
    sha256 = "0zms326j2bflnyvn57aiqg84wfp861dl78nng1gkgrqn554mr9h5";
  }) {}).pkgs;
  myPyPkgs = myPkgs.python3Packages.override {
    overrides = self: super: {
      impetuous-gfa = super.buildPythonPackage rec {
        pname = "impetuous-gfa";
        version = "0.13.0";
        src = super.fetchPypi {
          inherit pname version;
          sha256 = "1pmy8vkb5b3jnch3pz47b3022v4cjv6lx88lf3j9kidagzvjv1lw";
        };
        buildInputs = with super;
          [ pandas numpy statsmodels scikitlearn scipy patsy ];
      };
    };
  };
in

with myPkgs;
with lib;

stdenv.mkDerivation rec {
  name = "opls";
  buildInputs = (with myPyPkgs;
    [
      python impetuous-gfa scikitlearn
      scipy numpy pandas matplotlib bokeh
      statsmodels networkx ipython
    ]);
  shellHook = ''
    echo "*****************************"
    echo "* WELCOME TO ${toUpper name} SHELL *"
    echo "*****************************"
  '';
}
