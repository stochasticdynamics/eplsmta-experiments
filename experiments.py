# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.
import pandas as pd
import numpy as np

from impetuous.quantification import *
from impetuous.hierarchal import *
from impetuous.convert import *
from impetuous.clustering import *

pd.options.mode.chained_assignment = 'raise'
# Group definition files
data_dir = "../data"
convert_file = data_dir + "/naming_and_annotation/biomart/mart_export.txt"
ens2sym, sym2ens = create_synonyms(convert_file)

# Gene symbol to ensemble id mapping and reverse mapping
s2e = {**flatten_dict(sym2ens)}
e2s = {**flatten_dict(ens2sym)}

# The group definition files
pathway_gmt = data_dir + "/vanilla_reactome.gmt"
#   Dervied from v71 of reactome
#   Derived from HTA-2.0 annotations
tissue_gmt = data_dir + "/tissue_dictionary.gmt"
# Derived from v71 of reactome
compartment_gmt = data_dir + "/compartment_genes.gmt"
# Derived from v71 of reactome
parent_child_list = data_dir + "/ReactomeNodeRelations.txt"
reactome_ensembl_all = data_dir + "/Ensembl2Reactome_All_Levels_v71.txt"


def create_pls_association_map_data(
    mplsres, return_p_list=False, label="M,p", level=0.05
):
    # Determine the significance level
    sig_level = level
    directional_genes = (
        mplsres[0].iloc[(mplsres[0][label] < sig_level).values, :].copy()
    )
    directional_genes.index = directional_genes["name"].values

    # We must divide by the length scales
    xi = np.max(directional_genes[["x", "y"]])
    directional_genes.loc[:, "x"] = directional_genes["x"] / xi[0]
    directional_genes.loc[:, "y"] = directional_genes["y"] / xi[1]

    def angular_similarity(B, A):
        return np.dot(A, B) / (np.sqrt(np.dot(A, A)) * np.sqrt(np.dot(B, B)))

    feat_df_crd = directional_genes[["x", "y"]].copy()
    acorr_df = pd.DataFrame(
        np.zeros(len(feat_df_crd.index) * len(feat_df_crd.index)).reshape(
            len(feat_df_crd.index), len(feat_df_crd.index)
        ),
        index=feat_df_crd.index,
        columns=feat_df_crd.index,
    )
    for i_ in range(len(feat_df_crd.index)):
        for j_ in [a for a in range(i_, len(feat_df_crd.index))]:
            angle_corr = angular_similarity(
                feat_df_crd.iloc[i_].values, feat_df_crd.iloc[j_]
            )
            acorr_df.iloc[i_, j_] = angle_corr
            acorr_df.iloc[j_, i_] = angle_corr
    return acorr_df


def generate_reactome_gmt(
    fname="Ensembl2Reactome_All_Levels_v71.txt", gmt_file_name="vanilla_reactome.gmt"
):
    nodes = {}
    delimiter = "\t"
    pathways = {}
    with open(fname, "r") as input:
        for line in input:
            if "R-HSA" in line and "ENSG" in line:
                lsp = line.replace("\n", "").split(delimiter)
                if not lsp[1] in pathways:
                    pathways[lsp[1]] = [lsp[3], lsp[0]]
                else:
                    pathways[lsp[1]].append(lsp[0])

    # Write gmt file
    ofile = open(gmt_file_name, "w")
    for pathway in pathways.keys():
        print(pathway, delimiter, delimiter.join(pathways[pathway]), file=ofile)
    ofile.close()


def generate_MS_results():

    description = """Multiple Schlerosis data analysis"""

    name_str, journal_str, sep = (
        data_dir + "/analyte_MS.csv",
        data_dir + "/journal_MS.csv",
        "\t",
    )
    fdf = pd.read_csv(name_str, sep, index_col=0)

    analyte_df = drop_duplicate_indices(fdf)
    journal_df = pd.read_csv(journal_str, sep, index_col=0)

    n_cols = [
        ("Healthy" if "health" in v.lower() else "MS") + c
        for v, c in zip(journal_df.loc["StatusLabel"].values, journal_df.columns.values)
    ]
    analyte_df.columns = n_cols
    journal_df.columns = n_cols

    added_axis = ["Healthy", "Multiple Sclerosis"]

    # PLS analysis
    results_MS_status = run_rpls_regression(
        analyte_df,
        journal_df,
        "pls ~ C(StatusLabel)",
        synonyms=e2s,
        blur_cutoff=99.0,
        study_axii=[added_axis],
    )

    # Add foldchanges
    foldchange_indentifier = "FC,"
    results_fc = add_foldchanges(
        analyte_df,
        information_df=journal_df,
        group="StatusLabel",
        foldchange_indentifier=foldchange_indentifier,
    )

    output_tran_results = pd.concat(
        [
            results_MS_status[0].T,
            results_fc.loc[
                :,
                [
                    c
                    for c in results_fc.columns.values
                    if foldchange_indentifier in c[:3]
                ],
            ].T,
        ]
    ).T
    s_axis = "-".join(added_axis)
    check = s_axis
    output_tran_results[s_axis + " , -log10 of p value"] = [
        -np.log10(v) for v in output_tran_results.loc[:, s_axis + ",p"].values
    ]
    output_tran_results.sort_values(s_axis + ",p").to_excel(
        data_dir + "/supplement_ms-healthy_transcripts.xlsx"
    )
    results_MS_status[1].to_excel(data_dir + "/supplement_ms-healthy_patients.xlsx")

    # Create enrichment data
    str_check = "ms_data"

    # Create hierarchical enrichment graph
    dag_df, tree = create_dag_representation_df(
        pathway_file=pathway_gmt, pcfile=parent_child_list
    )
    hie_df = HierarchalEnrichment(
        results_MS_status[0],
        dag_df,
        dag_level_label="DAG,level",
        p_label=s_axis + ",p",
        ancestors_id_label="DAG,ancestors",
    )

    hie_df = hie_df.rename(columns={"Hierarchal,p": s_axis + ",p"})
    hie_df[check + ",q"] = [q[0] for q in qvalues(hie_df[s_axis + ",p"].values)]
    Frac_group, N_group = [], []
    for hidx, hids in zip(hie_df.index.values, hie_df["Included analytes,ids"].values):
        N = len(retrieve_genes_of(hidx, pathway_gmt))
        F = len(hids.split(",")) / N
        N_group.append(N)
        Frac_group.append(F)
    hie_df["FracGroupFill"] = Frac_group
    hie_df["NGroupAnalytes"] = N_group

    hie_df.sort_values(check + ",q").to_excel(
        data_dir
        + "/supplement_ms_"
        + str_check
        + "_hierarchal_enrichment_reactome.xlsx"
    )

    # Calculate the association map
    rtmp = results_MS_status[0]

    # Here we calculate q values with a constant fdr = 1 (from p values)
    rtmp.loc[:, "Healthy-Multiple Sclerosis,q"] = [
        q[0]
        for q in qvalues(results_MS_status[0]["Healthy-Multiple Sclerosis,p"].values)
    ]

    results_MS_status = [rtmp, results_MS_status[1]]
    association_map_df = create_pls_association_map_data(
        results_MS_status, level=0.16, label="Healthy-Multiple Sclerosis,q"
    )
    association_map_df.to_excel(data_dir + "/supplement_ms_association_map_data.xlsx")


def generate_TCGA_results():

    description = """TCGA-BRCA data analysis"""

    her2label = "HER2pos"
    name_str, journal_str, sep = (
        data_dir + "/analyte_TCGABRCA.csv",
        data_dir + "/journal_TCGABRCA.csv",
        "\t",
    )
    fdf = pd.read_csv(name_str, sep, index_col=0)

    analyte_df = drop_duplicate_indices(fdf).apply(lambda x: np.log2(x + 1))
    journal_df = pd.read_csv(journal_str, sep, index_col=0)

    # Create the triple negative labels
    simple_journal_df = journal_df.loc[
        [
            "StatusLabel",
            "TripleNegativeLabel",
            "HER2posLabel",
            "PRposLabel",
            "ERposLabel",
        ],
        :,
    ].copy()
    simple_journal_df.loc["StatusLabel"] = [
        v[0].upper() + v[1:] for v in simple_journal_df.loc["StatusLabel"].values
    ]
    simple_journal_df.loc["TripleNegativeLabel"] = [
        v[0].upper() + v[1:]
        for v in simple_journal_df.loc["TripleNegativeLabel"].values
    ]

    simple_journal_df = simple_journal_df.rename(
        index={v: v.split("Label")[0] for v in simple_journal_df.index.values}
    ).copy()
    for idx in simple_journal_df.index:
        if "pos" in idx:
            simple_journal_df.loc[idx] = [
                idx.split("pos")[0] + "p"
                if "p" == v[-1]
                else idx.split("pos")[0] + "n"
                if "n" == v[-1]
                else idx.split("pos")[0] + "~"
                for v in simple_journal_df.loc[idx].values
            ]

    # Create the triple positive and negative labels and check negative
    # Status
    triplepos = [
        hp[-1] == "p" and pp[-1] == "p" and ep[-1] == "p"
        for (hp, pp, ep) in zip(
            simple_journal_df.loc["HER2pos"],
            simple_journal_df.loc["PRpos"],
            simple_journal_df.loc["ERpos"],
        )
    ]
    simple_journal_df.loc["TripleStatus"] = [
        "TriplePositive" if tp else tn
        for (tn, tp) in zip(simple_journal_df.loc["TripleNegative"].values, triplepos)
    ]

    jdf = simple_journal_df.copy()
    old_journal_df = journal_df.copy()
    journal_df = jdf.copy()
    n_cols = [
        v + "-" + w + c.replace(".", "").replace("-", "")
        for w, v, c in zip(
            jdf.loc["TripleStatus"].values, jdf.loc["Status"].values, jdf.columns.values
        )
    ]
    analyte_df.columns = n_cols
    journal_df.columns = n_cols

    # Create the joined ER & PR status
    journal_df.loc["ERPR"] = [
        "ERpPRp"
        if (v == "PRp" and w == "ERp")
        else ("ERnPRn" if (v == "PRn" and w == "ERn") else "ER~PR~")
        for (v, w) in zip(journal_df.loc["PRpos"].values, journal_df.loc["ERpos"])
    ]

    n_cols = [
        w + "-" + v
        for w, v, c in zip(
            journal_df.loc["HER2pos"].values,
            journal_df.loc["ERPR"].values,
            journal_df.columns.values,
        )
    ]
    analyte_df.columns = n_cols
    journal_df.columns = n_cols

    added_axis = None

    # PLS analysis
    results_TCGA_status = run_rpls_regression(
        analyte_df,
        journal_df,
        "pls ~ C(ERPR) + C(HER2pos) + C(HER2pos):C(ERPR)",
        synonyms=e2s,
        blur_cutoff=95.0,
        owner_by="angle",
    )

    output_tran_results = results_TCGA_status[0]

    s_axis = "HER2p-ERnPRn"
    check = s_axis
    n_name_label = s_axis + " , -log10 of p value"
    output_tran_results[n_name_label] = [
        -np.log10(v) for v in output_tran_results.loc[:, s_axis + ",p"].values
    ]

    output_tran_results.sort_values(s_axis + ",p").to_excel(
        data_dir + "/supplement_tcga_" + check + "_transcripts.xlsx"
    )
    results_TCGA_status[1].to_excel(
        data_dir + "/supplement_tcga_" + check + "_patients.xlsx"
    )

    # Do the type-2 ANOVA comparison
    # This routing does not want string labels so we turn them into integers
    erprtonum = {
        v: (i + 1)
        for v, i in zip(
            set(journal_df.loc["ERPR"].values),
            range(len(set(journal_df.loc["ERPR"].values))),
        )
    }
    her2tonum = {
        v: (i + 1)
        for v, i in zip(
            set(journal_df.loc["HER2pos"].values),
            range(len(set(journal_df.loc["HER2pos"].values))),
        )
    }
    journal_df.loc["numERPR"] = [erprtonum[v] for v in journal_df.loc["ERPR"].values]
    journal_df.loc["numHER2"] = [her2tonum[v] for v in journal_df.loc["HER2pos"].values]
    qadf = quantify_analytes(
        analyte_df,
        journal_df.loc[["numERPR", "numHER2"], :],
        "pls ~ C(numERPR) + C(numHER2) + C(numHER2):C(numERPR)",
    )
    qadf.to_excel(data_dir + "/supplement_tcga_t2anova_results.xlsx")

    # Create the accociation map
    rtmp = results_TCGA_status[0]
    what_now = "HER2n-ERnPRn"
    rtmp.loc[:, what_now + ",q"] = [
        q[0] for q in qvalues(results_TCGA_status[0][what_now + ",p"].values)
    ]

    results_TCGA_status = [rtmp, results_TCGA_status[1]]
    association_map_df = create_pls_association_map_data(
        results_TCGA_status, level=0.001, label=what_now + ",p"
    )
    association_map_df.to_excel(data_dir + "/supplement_tcga_association_map_data.xlsx")


def generate_pls_tcga_p_o_q_results():
    pls = pd.read_excel(
        data_dir + "/supplement_tcga_HER2p-ERnPRn_transcripts.xlsx", index_col=0
    )
    what_now = "HER2p-ERnPRn,p"
    pls = pls.loc[
        :,
        [
            c
            for c in pls.columns
            if (",p" in c or ",q" in c) and (what_now.split(",")[0] in c)
        ],
    ].copy()
    pls_p = pls.loc[:, what_now].values

    def radial_pi(pv):
        return rankdata(pv, "average") / len(pv)

    pls.loc[:, "HER2+ & ER- , PR- : p"] = pls_p
    pls.loc[:, "HER2+ & ER- , PR- : o"] = [
        p * pi_r for p, pi_r in zip(pls_p, radial_pi(pls_p))
    ]
    pls.loc[:, "HER2+ & ER- , PR- : q"] = [
        q[0] for q in qvalues(pls.loc[:, "HER2+ & ER- , PR- : o"].values)
    ]
    pls.to_excel(data_dir + "/supplement_opq_tcga.xlsx")


def generate_broad_DM2_PCA_results():
    description = """MS PCA Comparative data analysis"""

    name_str, sep = data_dir + "/gsea_diabetes_df_ensemble.txt", "\t"
    fdf = pd.read_csv(name_str, sep, index_col=0)
    analyte_df = drop_duplicate_indices(fdf)

    print(analyte_df)

    # Cluster the T2D (DM2) samples
    udf = analyte_df.loc[:, [c for c in analyte_df.columns if "DM2" in c]]
    pcols = udf.columns.values

    cluster = Cluster(nclusters=2)
    cluster.approximate_density_clustering(udf.T)

    # Rename the cluster labels so that the largest comes first etc.
    cl_counts = sorted(
        [
            (np.sum([l == L for l in cluster.labels_]), L)
            for L in list(set([l for l in cluster.labels_]))
        ],
        reverse=True,
    )
    label_rename = {i_: sp[1] for sp, i_ in zip(cl_counts, range(len(cl_counts)))}
    cluster.labels_ = [label_rename[l] for l in cluster.labels_]

    N_DM2 = len([c for c in analyte_df.columns if "DM2" in c])
    ind_cats_r = {
        c: ":".join(c.split("_")[:2]) for c in analyte_df.columns if "DM2" in c
    }

    map_thresh = 5e-3
    axis_thres = 1e-4

    n_name = {d: "C" + str(1 + i) + "." + d for d, i in zip(pcols, cluster.labels_)}

    analyte_df = analyte_df.rename(columns=n_name)
    journal_df = pd.DataFrame(
        [[c.split("_")[0] for c in analyte_df.columns]],
        index=["Status"],
        columns=analyte_df.columns,
    )

    print(analyte_df)
    print(journal_df)

    basal_samples = analyte_df.columns
    from sklearn.decomposition import FastICA as PCA

    pca = PCA(2)
    scores_ = pca.fit_transform(analyte_df.loc[:, basal_samples].T)
    scores = scores_.T

    print(scores, np.shape(scores))
    print(pca.components_, np.shape(pca.components_))

    weights_df = pd.DataFrame(
        pca.components_.T, columns=["x", "y"], index=analyte_df.index.values
    )
    scores_df = pd.DataFrame(scores.T, columns=["x", "y"], index=basal_samples)

    # From the scores calc average score group position
    group_labels = [b.split("_")[0] for b in basal_samples]
    unique = sorted(list(set(group_labels)))
    centroid_positions = {}
    use_centroids = []
    use_labels = []
    for ulab in unique:
        centroid_position = (
            scores_df.loc[[c for c in scores_df.index.values if ulab in c], :]
            .apply(np.mean)
            .values
        )
        centroid_positions[ulab] = centroid_position
        use_labels.append(ulab)
        use_centroids.append(centroid_position)

    # owner_by == 'angle' :
    anglular_proximity = lambda B, A: 1 - np.dot(A, B) / (
        np.sqrt(np.dot(A, A)) * np.sqrt(np.dot(B, B))
    )
    feature_owner = [
        use_labels[np.argmin([anglular_proximity(xw, cent) for cent in use_centroids])]
        for xw in pca.components_.T
    ]

    weights_df.loc[:, "owner"] = feature_owner
    weights_df.loc[:, "name"] = [
        e2s[e] if e in e2s else e for e in analyte_df.index.values
    ]
    colors = ["#00ff00", "#00aaff", "#ff00aa", "#ff0000"]
    color_lookup = {l: c for (l, c) in zip(unique, colors)}
    weights_df.loc[:, "color"] = [
        color_lookup[o] for o in weights_df.loc[:, "owner"].values
    ]
    weights_df.loc[:, "alpha"] = [0.3 for a in range(len(weights_df))]
    print(weights_df)

    scores_df.loc[:, "owner"] = group_labels
    scores_df.loc[:, "color"] = [
        color_lookup[o] for o in scores_df.loc[:, "owner"].values
    ]
    scores_df.loc[:, "alpha"] = [0.3 for a in range(len(scores_df))]

    weights_df.to_excel(data_dir + "/supplement_dm2_pca_transcripts.xlsx")
    scores_df.to_excel(data_dir + "/supplement_dm2_pca_patients.xlsx")


def generate_broad_DM2_results():

    description = """Multiple Schlerosis data analysis"""

    name_str, sep = data_dir + "/gsea_diabetes_df_ensemble.txt", "\t"
    fdf = pd.read_csv(name_str, sep, index_col=0)
    analyte_df = drop_duplicate_indices(fdf)

    print(analyte_df)

    # Cluster the T2D (DM2) samples
    udf = analyte_df.loc[:, [c for c in analyte_df.columns if "DM2" in c]]
    pcols = udf.columns.values

    cluster = Cluster(nclusters=2)
    cluster.approximate_density_clustering(udf.T)

    # Rename the cluster labels so that the largest comes first etc.
    cl_counts = sorted(
        [
            (np.sum([l == L for l in cluster.labels_]), L)
            for L in list(set([l for l in cluster.labels_]))
        ],
        reverse=True,
    )
    label_rename = {i_: sp[1] for sp, i_ in zip(cl_counts, range(len(cl_counts)))}
    cluster.labels_ = [label_rename[l] for l in cluster.labels_]

    N_DM2 = len([c for c in analyte_df.columns if "DM2" in c])
    ind_cats_r = {
        c: ":".join(c.split("_")[:2]) for c in analyte_df.columns if "DM2" in c
    }

    map_thresh = 5e-3
    axis_thres = 1e-4

    n_name = {d: "C" + str(1 + i) + "." + d for d, i in zip(pcols, cluster.labels_)}

    analyte_df = analyte_df.rename(columns=n_name)
    journal_df = pd.DataFrame(
        [[c.split("_")[0] for c in analyte_df.columns]],
        index=["Status"],
        columns=analyte_df.columns,
    )

    analyte_df.to_excel("../data/used_dm2_analytes.xlsx")

    results_DM2_status = run_rpls_regression(
        analyte_df,
        journal_df,
        "pls ~ C(Status)",
        owner_by="angle",
        synonyms=e2s,
        blur_cutoff=99.0,
        study_axii=None,
    )

    labels = [
        ["Corr,p", "Association", " : p", map_thresh],
        ["C1.DM2,p", "C1.DM2-NGT", " : p", axis_thres],
    ]

    def radial_pi(pv):
        return np.max(pv) * rankdata(pv, "average") / len(pv)

    for label in labels:
        pls_p = results_DM2_status[0].loc[:, label[0]].values
        pls = results_DM2_status[0].loc[:, [label[0]]]
        mpt = label[3]

        pls.loc[:, label[1] + label[2]] = pls_p
        pls.loc[:, label[1] + " : o"] = [
            p * pi_r for p, pi_r in zip(pls_p, radial_pi(pls_p))
        ]
        pls.loc[:, label[1] + " : q"] = [
            q[0] for q in qvalues(pls.loc[:, label[1] + " : o"].values)
        ]

        results_DM2_status[0].loc[:, label[1] + " : q"] = pls.loc[:, label[1] + " : q"]

        p_df = create_pls_association_map_data(
            results_DM2_status, level=mpt, label=label[1] + " : q"
        )
        p_df.to_excel(
            data_dir + "/supplement_dm2_" + label[1] + "_association_map.xlsx"
        )

    results_DM2_status[0].to_excel(data_dir + "/supplement_dm2_transcripts.xlsx")
    results_DM2_status[1].to_excel(data_dir + "/supplement_dm2_patients.xlsx")


if __name__ == "__main__":
    generate_reactome_gmt(reactome_ensembl_all, pathway_gmt)
    generate_MS_results()
    generate_TCGA_results()
    generate_pls_tcga_p_o_q_results()
    generate_broad_DM2_results()
    generate_broad_DM2_PCA_results()
