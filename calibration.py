# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.
import pandas as pd
import numpy as np

from impetuous.quantification import *
from impetuous.convert import *

from visualization import create_figure_p
from bokeh.plotting import show,output_file

def foldchange_plot ( analyte_df , journal_df , axis_label = None ,
                      title = '' , dev = None ):
    if axis_label is None :
        print ( 'ERROR' )
        return
    if not dev is None :
        labels = axis_label.split('-')
        if not len(labels)==2:
            return(0)
        selection = { l:[] for l in labels }
        for idx in journal_df.index.values:
            for label in labels:
                isel = journal_df.columns.values[(journal_df.loc[idx].values == label)]
                if len(isel)>0 :
                    [ selection[label].append(i_) for i_ in isel ]
        sel1_ = list(set(selection[labels[0]]))
        sel2_ = list(set(selection[labels[1]]))
        m1_ = analyte_df.loc[:, sel1_ ].apply(np.mean,1)
        s1_ = analyte_df.loc[:, sel1_ ].apply(np.std ,1)
        m2_ = analyte_df.loc[:, sel2_ ].apply(np.mean,1)
        s2_ = analyte_df.loc[:, sel2_ ].apply(np.std ,1)
        n1_ = analyte_df.loc[:, sel1_ ].apply(len,1)
        n2_ = analyte_df.loc[:, sel2_ ].apply(len,1)
        df_ = (s1_**2/n1_+s2_**2/n2_)**2/( (s1_**2/n1_)**2/(n1_-1) + (s2_**2/n2_)**2/(n2_-1))
        print ( df_ )
    else :
        labels = axis_label.split('-')
        if not len(labels)==2:
            return(0)
        selection = { l:[] for l in labels }
        for idx in journal_df.index.values:
            for label in labels:
                isel = journal_df.columns.values[(journal_df.loc[idx].values == label)]
                if len(isel)>0 :
                    [ selection[label].append(i_) for i_ in isel ]
        sel1_ = list(set(selection[labels[0]]))
        sel2_ = list(set(selection[labels[1]]))
        import scipy.stats as stats_
        rdf = pd.DataFrame()
        for idx in analyte_df.index.values:
            a = analyte_df.loc[ idx, sel1_ ].dropna().values
            b = analyte_df.loc[ idx, sel2_ ].dropna().values
            if len(np.shape(a))>1:
                print(np.shape(a),a,idx);continue
            res_ = stats_.ttest_ind( a.reshape(-1), b.reshape(-1), equal_var=False )
            fc   = np.mean(a) - np.mean(b)
            rdf[idx] = [ fc , -np.log10(res_[1]) ,res_[1] , res_[0] ]

        lab0 , lab1 = 'Foldchange of '+axis_label,'-log10 of p value '
        result = rdf.T ; result.columns = [ lab0 , lab1 ,'p value' , 'test statistic' ]

        qdf=result

        qdf['name'] = qdf.index.values
        p = create_figure_p (   qdf , title_label = title ,
                                xaxis_label = lab0 , yaxis_label = lab1,
                                name_xvals = lab0 , name_yvals = lab1 )
        return result,p

if __name__=='__main__' :
    data_dir = "../data"
    print ( 'RUN AFTER EXPERIMENTS')

    if True :
        print ( 'CALIBRATION OF MS RESULTS' )
        from bokeh.models import Row
        from bokeh.models import ColumnDataSource,Line
        added_axis = ["Healthy","Multiple Sclerosis"]

        name_str, journal_str, sep = (
            data_dir + "/analyte_MS.csv",
            data_dir + "/journal_MS.csv",
            "\t",
        )
        fdf = pd.read_csv(name_str, sep, index_col=0)

        analyte_df = drop_duplicate_indices(fdf)
        journal_df = pd.read_csv(journal_str, sep, index_col=0)

        n_cols = [
            ("Healthy" if "health" in v.lower() else "MS") + c
            for v, c in zip(journal_df.loc["StatusLabel"].values, journal_df.columns.values)
        ]
        analyte_df.columns = n_cols
        journal_df.columns = n_cols

        opls_df_ = pd.read_excel('../data/supplement_ms-healthy_transcripts.xlsx',index_col=0)
        p_values_opls = [ -np.log10(p) for p in opls_df_.loc[:,'Healthy-Multiple Sclerosis,p'].values ]

        rdf,fc_p_ = foldchange_plot ( analyte_df.loc[opls_df_.index.values,:] , journal_df , axis_label= '-'.join(added_axis) )
        p_values_ttest = [ -np.log10(p) for p in rdf.loc[:,'p value'].values ]

        p_cal = create_figure_p (   pd.DataFrame([p_values_opls,p_values_ttest] ,
                                         index = ['opls p values','ttest p values'],
                                         columns = opls_df_.index.values ).T ,
                            title_label = 'Calibration' ,
                            xaxis_label = 'ttest -log10( p values )' , yaxis_label = 'opls -log10( p values )',
                            name_xvals = 'ttest p values' , name_yvals = 'opls p values' )

        source = ColumnDataSource(dict(x=[0,10], y=[0,5]))
        a_ = 1./np.sqrt( len(analyte_df.columns)*0.5/np.pi )
        y_ = lambda x,a_ : x*a_
        p_cal.line(x=[0,10], y=[0,y_(10,a_)], line_color="#f46d43", line_width=6, line_alpha=0.6 , legend='y = '+str(a_)[:5]+'*x')
        show(Row(fc_p_,p_cal))
        #
        # IN THIS CASE WE ARE PERFOMING ON PAR WITH A TTEST
        # PVALUES ARE HETEROSCEDASTICALLY DISTRIBUTED AROUND HALF THE POWER LINE. IN THIS CASE IT IS CLEAR
        # THAT OUR P VALUES ARE ROUGHLY THE SQUARE ROOT OF THE TTEST PVALUES AND WE ARE AS SUCH CONSERVATIVE
        #

    if True :
        print ( 'CALIBRATION OF TCGA MAIN RESULT' )
        from bokeh.models import Row
        from bokeh.models import ColumnDataSource,Line

        ###
        her2label = "HER2pos"
        name_str, journal_str, sep = (
            data_dir + "/analyte_TCGABRCA.csv",
            data_dir + "/journal_TCGABRCA.csv",
            "\t",
        )
        fdf = pd.read_csv(name_str, sep, index_col=0)

        analyte_df = drop_duplicate_indices(fdf).apply(lambda x: np.log2(x + 1))
        journal_df = pd.read_csv(journal_str, sep, index_col=0)

        # Create the triple negative labels
        simple_journal_df = journal_df.loc[
            [
                "StatusLabel",
                "TripleNegativeLabel",
                "HER2posLabel",
                "PRposLabel",
                "ERposLabel",
            ],
            :,
        ].copy()
        simple_journal_df.loc["StatusLabel"] = [
            v[0].upper() + v[1:] for v in simple_journal_df.loc["StatusLabel"].values
        ]
        simple_journal_df.loc["TripleNegativeLabel"] = [
            v[0].upper() + v[1:]
            for v in simple_journal_df.loc["TripleNegativeLabel"].values
        ]

        simple_journal_df = simple_journal_df.rename(
            index={v: v.split("Label")[0] for v in simple_journal_df.index.values}
        ).copy()
        for idx in simple_journal_df.index:
            if "pos" in idx:
                simple_journal_df.loc[idx] = [
                    idx.split("pos")[0] + "p"
                    if "p" == v[-1]
                    else idx.split("pos")[0] + "n"
                    if "n" == v[-1]
                    else idx.split("pos")[0] + "~"
                    for v in simple_journal_df.loc[idx].values
                ]

        # Create the triple positive and negative labels and check negative
        # Status
        triplepos = [
            hp[-1] == "p" and pp[-1] == "p" and ep[-1] == "p"
            for (hp, pp, ep) in zip(
                simple_journal_df.loc["HER2pos"],
                simple_journal_df.loc["PRpos"],
                simple_journal_df.loc["ERpos"],
            )
        ]
        simple_journal_df.loc["TripleStatus"] = [
            "TriplePositive" if tp else tn
            for (tn, tp) in zip(simple_journal_df.loc["TripleNegative"].values, triplepos)
        ]

        jdf = simple_journal_df.copy()
        old_journal_df = journal_df.copy()
        journal_df = jdf.copy()
        n_cols = [
            v + "-" + w + c.replace(".", "").replace("-", "")
            for w, v, c in zip(
                jdf.loc["TripleStatus"].values, jdf.loc["Status"].values, jdf.columns.values
            )
        ]
        analyte_df.columns = n_cols
        journal_df.columns = n_cols

        # Create the joined ER & PR status
        journal_df.loc["ERPR"] = [
            "ERpPRp"
            if (v == "PRp" and w == "ERp")
            else ("ERnPRn" if (v == "PRn" and w == "ERn") else "ER~PR~")
            for (v, w) in zip(journal_df.loc["PRpos"].values, journal_df.loc["ERpos"])
        ]

        n_cols = [
            w + "-" + v
            for w, v, c in zip(
                journal_df.loc["HER2pos"].values,
                journal_df.loc["ERPR"].values,
                journal_df.columns.values,
            )
        ]
        analyte_df.columns = n_cols
        journal_df.columns = n_cols

        added_axis = ['HER2n&ERnPRn','HER2n&ERpPRp']

        opls_df_ = pd.read_excel('../data/supplement_tcga_HER2p-ERnPRn_transcripts.xlsx',index_col=0)

        indices = sorted(list(set(opls_df_.index.values)&set(analyte_df.index.values)))
        analyte_df = analyte_df.loc[indices,:]
        opls_df_   = opls_df_  .loc[indices,:]
        #
        # IN THE OPLS MODEL THE 'HER2p-ERnPRn' INSTANCE HAS ENOUGHT WEIGHT TO SET THE DIRECTION
        p_values_opls = [ -np.log10(p) for p in opls_df_.loc[:,added_axis[0].replace('&','-')+',p'].values ]
        #
        # ADDING THE CORRECT FC GROUP (WE LEARNED IT FROM OPLS ALIGNMENT)
        journal_df.loc['FCG'] = [ '&'.join([h,ep]) for h,ep in zip(journal_df.loc['HER2pos'].values,journal_df.loc['ERPR'].values) ]
        #
        rdf,fc_p_  = foldchange_plot ( analyte_df, journal_df , axis_label= '-'.join(added_axis) )
        #
        p_values_ttest = [ -np.log10(p) for p in rdf.loc[:,'p value'].values ]

        p_cal = create_figure_p (   pd.DataFrame([p_values_opls,p_values_ttest,analyte_df.index.values] ,
                                         index = ['opls p values','ttest p values','name'],
                                         columns = opls_df_.index.values ).T ,
                            title_label = 'Calibration' ,
                            xaxis_label = 'ttest -log10( p values )' , yaxis_label = 'opls -log10( p values )',
                            name_xvals = 'ttest p values' , name_yvals = 'opls p values' )

        #source = ColumnDataSource(dict(x=[0,10], y=[0,5]))
        a_ = 1./np.sqrt( len(analyte_df.columns)*0.5/np.pi )
        y_ = lambda x,a_ : x*a_
        p_cal.line(x=[0,80], y=[0,y_(80,a_)], line_color="#f46d43", line_width=6, line_alpha=0.6 , legend='y = '+str(a_)[:5]+'*x')

        # p_cal.line(x=[0,10], y=[0,5], line_color="#f46d43", line_width=6, line_alpha=0.6 , legend='y = 0.5*x')
        # p_cal.line(x=[0,10], y=[0,20], line_color="#f46d43", line_width=6, line_alpha=0.6 , legend='y = 2.0*x')
        show(Row(fc_p_,p_cal))
