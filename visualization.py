# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.
import gc, os, warnings, math

import pandas as pd
import numpy as np

from scipy import stats
from scipy.stats import rankdata
import statsmodels.api as sm
from impetuous.quantification import qvalues

import matplotlib._color_data as mcd
import matplotlib.pyplot as plt

from bokeh.layouts import row, layout, widgetbox, column
from bokeh.models import HoverTool, Range1d, Text, Row
from bokeh.models import (
    CustomJS,
    Div,
    ColumnDataSource,
    HoverTool,
    Circle,
    Range1d,
    DataRange1d,
    Text,
    Row,
)
from bokeh.models import Arrow, OpenHead, NormalHead, VeeHead, Line
from bokeh.models import MultiSelect
from bokeh.models.widgets import TextInput, Toggle
from bokeh.plotting import figure, output_file, show, save, ColumnDataSource

data_dir = "../data"
plot_dir = "../plots"


def histogram_plot(
    nbins=100,
    color=None,
    label=None,
    plot_pane_width=500,
    plot_pane_height=500,
    title="",
):
    p = figure(
        background_fill_color="white",
        title=title,
        plot_width=plot_pane_width,
        plot_height=plot_pane_height,
    )
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = "white"
    p.grid.grid_line_width = 2
    return p


def add_histogram_data(hist_source, bfig, label=None, color=None, alpha=None):
    if "dict" in str(type(hist_source)):
        single_hist_dict = dict()
        single_hist_dict["hist"] = hist_source[label + "hist"]
        single_hist_dict["edges_r"] = hist_source[label + "edges"][1:]
        single_hist_dict["edges_l"] = hist_source[label + "edges"][:-1]
        single_hist_source = single_hist_dict
    else:
        single_hist_source = hist_source
    if "None" in str(type(color)):
        color = "#036564"
    lcolor = "black"
    bfig.quad(
        top="hist",
        bottom=0,
        left="edges_l",
        right="edges_r",
        legend=label,
        fill_color=color,
        line_color=lcolor,
        source=single_hist_source,
        alpha=alpha,
    )


def make_histogram_dict(df, nbins=50, label=None, axis=1, hrange=None):
    hist_dict = dict()
    if axis == 1:
        hist, edges = np.histogram(df[label], density=True, bins=nbins, range=hrange)
    else:
        hist, edges = np.histogram(
            df.loc[label], density=True, bins=nbins, range=hrange
        )
    hist_dict[label + "hist"] = hist
    hist_dict[label + "edges"] = edges
    return hist_dict


def bokeh_histogram_basic(
    df,
    nbins=50,
    labels=["p", "q"],
    colors=["green", "purple"],
    title=None,
    filename=None,
    backend="webgl",
    bReturnPlot=False,
    plot_pane_width=500,
    plot_pane_height=500,
):

    p = histogram_plot(
        title=title, plot_pane_width=plot_pane_width, plot_pane_height=plot_pane_height
    )

    for i in range(len(labels)):
        label = labels[i]
        hist_dict = make_histogram_dict(
            df, nbins=nbins, label=label, axis=1, hrange=(0.0, 1.0)
        )
        add_histogram_data(hist_dict, label=label, color=colors[i], bfig=p, alpha=0.3)
    p.output_backend = backend

    if bReturnPlot:
        return p

    if not "None" in str(type(filename)):
        output_file(filename)
        save(p)
    else:
        show(p)


def show_tcga_opg_histogram_plot():
    pls = pd.read_excel(
        data_dir + "/supplement_tcga_HER2p-ERnPRn_transcripts.xlsx", index_col=0
    )
    pls = pls.loc[
        :, [c for c in pls.columns if ",p" in c or ",q" in c or ",r" in c]
    ].copy()
    pls_p = pls.loc[:, "HER2p-ERnPRn,p"].values
    rho = pls.loc[:, "HER2p-ERnPRn,r"]
    print(rho[:10], pls_p[:10])  # ; exit(1)

    def radial_pi(pv):
        return rankdata(pv, "average") / len(pv)

    pls.loc[:, "HER2+ & ER- , PR- : p"] = pls_p
    pls.loc[:, "HER2+ & ER- , PR- : o"] = [
        p * pi_r for p, pi_r in zip(pls_p, radial_pi(pls_p))
    ]  # [ phat(p,r) for p,r in zip(pls_p,rho) ]
    pls.loc[:, "HER2+ & ER- , PR- : q"] = [
        q[0] for q in qvalues(pls.loc[:, "HER2+ & ER- , PR- : o"].values)
    ]
    print(sorted(pls.loc[:, "HER2+ & ER- , PR- : q"])[:10])

    P = []
    use_colors = ["gray", "red", "blue"]
    names = ["HER2+ & ER- , PR- : p", "HER2+ & ER- , PR- : o", "HER2+ & ER- , PR- : q"]
    for i_ in range(len(names)):
        name = names[i_]
        P.append(
            bokeh_histogram_basic(
                pls,
                labels=[name],
                colors=[use_colors[i_]],
                backend="webgl",
                bReturnPlot=True,
                plot_pane_width=600,
                plot_pane_height=200,
            )
        )
    show(column(*P))


# This messy routine is for drawing the boxplots and it is using seaborn
def draw_gene_boxplot(
    label,
    afile=data_dir + "/data/analyte_df.csv",
    jfile=data_dir + "/data/journal_df.csv",
    ofile=data_dir + "/output.png",
    non_adjoint_covariate_groups=None,
    color_by=None,
    bLog2=False,
    genelist=["ENSG00000161267"],
    grid=[1, 1],
    wspace=0.5,
    only_show=None,
    colors=None,
    rotate_labels=45,
    convert_file=data_dir + "/data/biomart/mart_export.txt",
):
    import matplotlib.pyplot as plt
    import seaborn as sns

    # Find and build gene-id mapping
    e2s = None
    if not convert_file is None:
        ens2sym, sym2ens = create_synonyms(convert_file)
        s2e = {**flatten_dict(sym2ens)}
        e2s = {**flatten_dict(ens2sym)}

    if "str" in str(type(afile)):
        analyte_df = pd.read_csv(afile, "\t", index_col=0)
    else:
        analyte_df = afile

    if "str" in str(type(jfile)):
        journal_df = pd.read_csv(jfile, "\t", index_col=0)
    else:
        journal_df = jfile

    analyte_df = analyte_df.loc[:, journal_df.columns.values]
    if not only_show is None:
        analyte_df = analyte_df.iloc[
            :, [v == only_show[1] for v in journal_df.loc[only_show[0]].values]
        ]
        journal_df = journal_df.iloc[
            :, [v == only_show[1] for v in journal_df.loc[only_show[0]].values]
        ]

    bVerbose = True

    fc_between = list(set(journal_df.loc[label].values))
    if bLog2:
        analyte_df = analyte_df.apply(lambda x: np.log2(1 + x))
    status = journal_df.loc[
        label
    ].values  # [ c.split('-')[0] for c in analyte_df.columns ]

    bRestrict = False
    colored = None
    if not color_by is None:
        levels = set(journal_df.loc[color_by].values)
        labels = journal_df.loc[color_by].values
        I_ = 0
        value_map = {}
        for i in range(len(labels)):
            if labels[i] in levels:
                value_map[labels[i]] = I_
                I_ += 1
                levels = levels - set([labels[i]])
        int_levels = [value_map[l] for l in labels]
        colored = labels
    I_ = 0
    postfix = ""
    plt.subplots_adjust(wspace=wspace)
    if not colored is None:
        fin_col_label = "Color"
    else:
        fin_col_label = None

    for gene in genelist:
        symbol = gene
        if not e2s is None:
            if gene in e2s:
                symbol = e2s[gene]
            else:
                symbol = gene
        new_df = analyte_df.loc[[gene], :].apply(pd.to_numeric).copy()
        if not colored is None:
            new_df.loc[fin_col_label] = colored

        new_df.loc[label] = status
        new_df.loc["Sample"] = new_df.columns.values

        if bRestrict:
            new_df = new_df.iloc[
                :, [("basal" in c) for c in new_df.loc["Sample"].values]
            ]
            new_df.columns = [
                "_" + c.split("_")[1].split("-")[1] + "_0"
                for c in new_df.columns.values
            ]

        if colors is None:  # ['#aaff00','#ff00aa']
            colors = ["#018571", "#ca0020"]  # ['#aaff00','#ff00aa']
        new_df = new_df.T
        new_df = new_df.rename(columns={gene: symbol + postfix})
        new_df.index = range(len(new_df))
        sns.set_context(
            "poster",
            font_scale=1.5,
            rc={"lines.linewidth": 3, "lines.linecolor": "blue"},
        )
        plt.subplot(grid[0], grid[1], I_ + 1)
        new_df[symbol + postfix] = new_df[symbol + postfix].astype(float)
        # print ( new_df , label , symbol+postfix , fin_col_label )
        ax = sns.boxplot(
            x=label,
            y=symbol + postfix,
            hue=fin_col_label,
            data=new_df,
            palette=[sns.saturate(colors[0]), sns.saturate(colors[1])],
        )
        ax.set_xticklabels(labels=ax.xaxis.get_ticklabels(), rotation=30)
        if I_ < len(genelist) - 1 and (not fin_col_label is None):
            ax.legend_.remove()
        I_ += 1

    fig = plt.gcf()
    bw_ = 30.0
    if not fin_col_label is None:
        plt.legend(bbox_to_anchor=(1.5, 0.0, 0.0, 0.0))
    fig.set_size_inches(grid[0] * bw_, grid[1] * bw_)
    plt.savefig(
        ofile,
        dpi=60,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        papertype=None,
        format=None,
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
        frameon=None,
        metadata=None,
    )
    plt.show()


from scipy.cluster.hierarchy import linkage, dendrogram


def heatmap(
    pdf,
    return_p_list=False,
    o_map_name="association_map.html",
    title="",
    text_format={"label_size": 10},
    size=600,
):

    Z = linkage(pdf.values, "ward")
    dn = dendrogram(Z)
    iord = [int(i) for i in dn["ivl"]]
    pdf = pdf.iloc[iord, iord]

    xname, yname = [], []

    for i in pdf.columns:
        for j in pdf.index:
            xname.append(i)
            yname.append(j)

    data_dir_colors = [(255, 0, 0), (255, 255, 255), (0, 0, 255)]
    ncols = 10
    hexcol = lambda c: "#%02x%02x%02x" % (c[0] % 256, c[1] % 256, c[2] % 256)
    hexreds = lambda c: "#%02x%02x%02x" % (255, c[0] % 256, c[1] % 256)
    hexblus = lambda c: "#%02x%02x%02x" % (c[0] % 256, c[1] % 256, 255)
    cmin = -1.0
    cmax = 1.0
    x2short = lambda x: int(np.ceil((((1 - np.abs(x))) * 255)))

    color = [
        hexreds((x2short(v), x2short(v)))
        if v > 0
        else hexblus((x2short(v), x2short(v)))
        for v in pdf.values.reshape(-1)
    ]
    alpha = [0.6 if v else 0.6 for v in pdf.apply(lambda x: x > 0).values.reshape(-1)]
    counts = np.array([v for v in pdf.values.reshape(-1)])
    names = pdf.columns.values

    data = dict(
        xname=xname, yname=yname, colors=color, alphas=alpha, count=counts.flatten()
    )

    p = figure(
        title=title,
        x_axis_location="above",
        tools="hover,save,zoom_in,zoom_out,reset,box_zoom",
        x_range=list(reversed(names)),
        y_range=names,
        tooltips=[("names", "@yname, @xname"), ("count", "@count")],
    )

    p.plot_width = size
    p.plot_height = size
    p.grid.grid_line_color = None
    p.axis.axis_line_color = None
    p.axis.major_tick_line_color = None
    p.axis.major_label_text_font_size = str(text_format["label_size"]) + "pt"
    p.axis.major_label_standoff = 0
    p.xaxis.major_label_orientation = np.pi / 3

    p.rect(
        "xname",
        "yname",
        0.9,
        0.9,
        source=data,
        color="colors",
        alpha="alphas",
        line_color=None,
        hover_line_color="black",
        hover_color="colors",
    )

    output_file(o_map_name, title=title)
    if return_p_list:
        return [p]
    else:
        show(p)


def create_figure_p(
    data_df,
    title_label="",
    name_xvals="x",
    name_yvals="y",
    xaxis_label="",
    yaxis_label="",
):

    plot_dimensions = [600, 600]
    icon_size, icon_alpha = 10, 0.6
    dp_ = 100
    global_spot_size = 14
    global_spot_alpha = 0.33
    major_label_text_font_size = ["20pt", "20pt"]
    minor_label_text_font_size = ["18pt", "18pt"]
    major_box_label_orientation = [0.0, 0.0]
    textfont, textsize, textstyle, textangle = "Arial", "10pt", "bold", 0.0

    xmax = np.max(data_df[name_xvals])
    xmin = np.min(data_df[name_xvals])
    ymax = np.max(data_df[name_yvals])
    ymin = np.min(data_df[name_yvals])

    ttips = [("index ", "$index"), ("(x,y) ", "(@x, @y)"), ("name ", "@name")]

    if "owner" in set(data_df.columns.values):
        ttips.append(("proximal to", "@owner"))

    hover = HoverTool(tooltips=ttips)

    p = figure(
        plot_width=plot_dimensions[0],
        plot_height=plot_dimensions[1],
        tools=[hover, "box_zoom", "wheel_zoom", "pan", "reset", "save"],
        title=title_label,
        x_range=(xmin - np.sign(xmin) * 0.1 * xmin, xmax + np.sign(xmax) * 0.1 * xmax),
        y_range=(ymin - np.sign(ymin) * 0.1 * ymin, ymax + np.sign(ymax) * 0.1 * ymax),
    )

    bGlyphs = False
    if bGlyphs:
        p.add_glyph(cds, glyph)

    text = None
    p.outline_line_width = 0
    p.outline_line_alpha = 0.0
    p.outline_line_color = "white"

    if "color" in set(data_df.columns.values) and "alpha" in set(
        data_df.columns.values
    ):

        # For creating the legend points
        sdf = data_df.loc[
            [
                data_df.iloc[data_df["color"].values == col, :].index.values[0]
                for col in set(data_df["color"].values)
            ],
            :,
        ].copy()

        # Make sure only one entry per label is kept
        if "label" in set(data_df.columns.values):
            sdf.index = sdf["label"].values
            from impetuous.convert import drop_duplicate_indices

            sdf = drop_duplicate_indices(sdf)

        sdf.index = [i for i in range(len(set(data_df["color"].values)))]

        sdf["alpha"] = 1.0
        sdf[name_xvals] = sdf[name_xvals].values + xmax * 100
        sdf[name_yvals] = sdf[name_yvals].values + ymax * 100
        data_df = pd.concat([sdf, data_df])
        source = ColumnDataSource(data=data_df)

        if "label" in set(data_df.columns.values):
            p.circle(
                name_xvals,
                name_yvals,
                size=12,
                source=source,
                color="color",
                alpha="alpha",
                legend="label",
            )
        else:
            p.circle(
                name_xvals,
                name_yvals,
                size=12,
                source=source,
                color="color",
                alpha="alpha",
            )
    else:
        source = ColumnDataSource(data=data_df)
        p.circle(name_xvals, name_yvals, size=12, source=source, alpha=0.25)

    if False:
        p.line(xsigrange, [siglevel, siglevel], line_width=2, color="black", alpha=1.0)

    p.xaxis.axis_label = xaxis_label
    p.yaxis.axis_label = yaxis_label

    p.xgrid.grid_line_color = "white"
    p.ygrid.grid_line_color = "white"
    p.grid.grid_line_width = 0
    p.title.text_font_size = major_label_text_font_size[0]
    p.xaxis.axis_label_text_font_size = major_label_text_font_size[0]
    p.yaxis.axis_label_text_font_size = major_label_text_font_size[1]
    p.xaxis.major_label_text_font_size = minor_label_text_font_size[0]
    p.yaxis.major_label_text_font_size = minor_label_text_font_size[1]
    p.output_backend = "webgl"
    return p


def show_results(
    result_dfs,
    use_colors_dict=None,
    rename_labels=None,
    titles=None,
    xlabels=None,
    ylabels=None,
    default_names="owner",
):
    PP = []
    for I_ in range(len(result_dfs)):
        print("SHOWING:", I_)
        qdf = result_dfs[I_]
        if not use_colors_dict is None:
            if False:
                print(use_colors_dict)
                print(use_colors_dict[I_], set(qdf[default_names].values))
                print([use_colors_dict[I_][i] for i in qdf[default_names].values])
                print(I_, len(result_dfs))
                print(len(default_names), len(qdf))
                print(set(qdf[default_names].values))
            qdf["color"] = [use_colors_dict[I_][i] for i in qdf[default_names].values]
        else:
            qdf["color"] = ["#000000" for i in qdf[default_names].values]
        if rename_labels is None:
            qdf["label"] = qdf[default_names].values
        else:
            qdf["label"] = [
                rename_labels[v] if v in rename_labels else v
                for v in qdf[default_names].values
            ]
        if titles is None:
            title = ""
        else:
            title = titles[I_]
        if xlabels is None:
            xlab = ""
        else:
            xlab = xlabels[I_]
        if ylabels is None:
            ylab = ""
        else:
            ylab = ylabels[I_]
        PP.append(
            create_figure_p(qdf, title_label=title, xaxis_label=xlab, yaxis_label=ylab)
        )
    show(Row(*PP))

def make_pls_anova_comparison_plot(features = {}, samples = {}):
    qadf = pd.read_excel(
        data_dir + "/supplement_tcga_t2anova_results.xlsx", index_col=0
    )  # _excel('./t2anova_results.xlsx',index_col=0)
    rtcgafeat=features
    rtcgas=samples
    print([c for c in qadf.columns if ",q" in c])
    t2anova_sig = qadf.iloc[
        [v < 0.05 / len(qadf) for v in qadf.loc[:, "C(numHER2):C(numERPR),q"].values], :
    ].index.values
    new_alpha = [
        0.95 * int(v < 0.05 / len(qadf)) + 0.05
        for v in qadf.loc[:, "C(numHER2):C(numERPR),q"].values
    ]
    print(t2anova_sig)
    qadf.loc[:, "alpha"] = new_alpha
    print(qadf["alpha"])

    rtcgafeat["alpha"] = qadf["alpha"]

    show_results(
        [rtcgafeat,rtcgas],
        use_colors_dict=[hormone_colors, hormone_colors],
        titles={0: "FEATURE WEIGHTS", 1: "SAMPLE SCORES"},
        xlabels={
            0: "Transcript weight, first component",
            1: "Sample score, first component",
        },
        ylabels={
            0: "Transcript weight, second component",
            1: "Sample score, second component",
        },
    )


if __name__ == "__main__":

    # Create the dual PLS plots
    results_MS_status = [
        pd.read_excel(
            data_dir + "/supplement_ms-healthy_transcripts.xlsx", index_col=0
        ),
        pd.read_excel(data_dir + "/supplement_ms-healthy_patients.xlsx", index_col=0),
    ]

    # We source in a FC plot
    xaxis_label = [c for c in results_MS_status[0].columns.values if "FC," in c[:3]][0]
    yaxis_label = "Healthy-Multiple Sclerosis , -log10 of p value"
    output_file(plot_dir + "/MS_FC.html")
    show(
        create_figure_p(
            results_MS_status[0],
            title_label="",
            name_xvals=xaxis_label,
            xaxis_label="log2 FC of " + xaxis_label.split(",")[1],
            name_yvals=yaxis_label,
            yaxis_label=yaxis_label,
        )
    )

    output_file(plot_dir + "/MS_OPLS.html")
    show_results(
        results_MS_status,
        use_colors_dict=[
            {"Multiple Sclerosis": "#ca0020", "Healthy": "#018571"},
            {"MS": "#ca0020", "Healthy": "#018571"},
        ],
        rename_labels={"MS": "Multiple Sclerosis"},
        titles={0: "FEATURE WEIGHTS", 1: "SAMPLE SCORES"},
        xlabels={
            0: "Transcript weight, first component",
            1: "Sample score, first component",
        },
        ylabels={
            0: "Transcript weight, second component",
            1: "Sample score, second component",
        },
    )

    # TCGA analysis results
    results_TCGA_status = [
        # tcgafeat
        pd.read_excel(
            data_dir + "/supplement_tcga_HER2p-ERnPRn_transcripts.xlsx", index_col=0
        ),
        # tcgas
        pd.read_excel(
            data_dir + "/supplement_tcga_HER2p-ERnPRn_patients.xlsx", index_col=0
        ),
    ]

    hormone_colors = {
        "PRp": "#6a3d9a",
        "PRn": "#b15928",
        "PR~": "#8dd3c7",
        "ERp": "#ffff99",
        "ERn": "#a1d76a",
        "ER~": "#ffffb3",
        "HER2p": "#f1a340",
        "HER2n": "#998ec3",
        "HER2~": "#bebada",
        "TripleNegative": "#fb8072",
        "TriplePositive": "#80b1d3",
        "Regular": "#fdb462",
        "ERpPRp": "#e9a3c9",
        "ERnPRn": "#e9a3c9",
        "HER2~": "#00aa00",
        "ER~PR~": "#d8b365",
        "HER2~-ERpPRp": "#a6cee3",
        "HER2~-ERnPRn": "#1f78b4",
        "HER2~-ER~PR~": "#b2df8a",
        "HER2n-ERpPRp": "#33a02c",
        "HER2n-ERnPRn": "#fb9a99",
        "HER2n-ER~PR~": "#e31a1c",
        "HER2p-ERpPRp": "#fdbf6f",
        "HER2p-ERnPRn": "#ff7f00",
        "HER2p-ER~PR~": "#cab2d6",
    }

    renaming_dict = {
        "ERp": "ER+",
        "ERn": "ER-",
        "PRp": "PR+",
        "PRn": "PR-",
        "HER2n": "HER2-",
        "HER2p": "HER2+",
        "ERpPRp": "ER+,PR+",
        "ERnPRn": "ER- & PR-",
        "HER2~-ERpPRp": "HER2~ & ER+,PR+",
        "HER2~-ERnPRn": "HER2~ & ER-,PR-",
        "HER2~-ER~PR~": "HER2~ & ER~,PR~",
        "HER2n-ERpPRp": "HER2- & ER+,PR+",
        "HER2n-ERnPRn": "HER2- & ER-,PR-",
        "HER2n-ER~PR~": "HER2- & ER~,PR~",
        "HER2p-ERpPRp": "HER2+ & ER+,PR+",
        "HER2p-ERnPRn": "HER2+ & ER-,PR-",
        "HER2p-ER~PR~": "HER2+ & ER~,PR~",
    }

    output_file(plot_dir + "/TCGA_OPLS.html")
    show_results(
        results_TCGA_status,
        use_colors_dict=[hormone_colors, hormone_colors],
        rename_labels=renaming_dict,
        titles={0: "FEATURE WEIGHTS", 1: "SAMPLE SCORES"},
        xlabels={
            0: "Transcript weight, first component",
            1: "Sample score, first component",
        },
        ylabels={
            0: "Transcript weight, second component",
            1: "Sample score, second component",
        },
    )

    # Create the dual PLS plots
    results_D_status = [
        pd.read_excel(data_dir + "/supplement_dm2_transcripts.xlsx", index_col=0),
        pd.read_excel(data_dir + "/supplement_dm2_patients.xlsx", index_col=0),
    ]

    output_file(plot_dir + "/DM2_OPLS.html")
    def_colors = {
        "DM2": "#ca0020",
        "C1.DM2": "#ca0020",
        "C2.DM2": "#0000ff",
        "C3.DM2": "#ffff00",
        "C4.DM2": "#0aff0a",
        "NGT": "#018571",
    }
    def_colors = {**def_colors, **{"C5.DM2": "#fafffa"}}
    show_results(
        results_D_status,
        use_colors_dict=[def_colors, def_colors],
        rename_labels=None,
        titles={0: "FEATURE WEIGHTS", 1: "SAMPLE SCORES"},
        xlabels={
            0: "Transcript weight, first component",
            1: "Sample score, first component",
        },
        ylabels={
            0: "Transcript weight, second component",
            1: "Sample score, second component",
        },
    )

    # Here we produce a PCA dual plot
    r1 = pd.read_excel(data_dir + "/supplement_dm2_pca_transcripts.xlsx", index_col=0)
    r2 = pd.read_excel(data_dir + "/supplement_dm2_pca_patients.xlsx", index_col=0)
    output_file(plot_dir + "/DM2_PCA.html")
    show_results(
        [r1, r2],
        use_colors_dict=[def_colors, def_colors],
        rename_labels=None,
        titles={0: "FEATURE WEIGHTS", 1: "SAMPLE SCORES"},
        xlabels={
            0: "Transcript weight, first component",
            1: "Sample score, first component",
        },
        ylabels={
            0: "Transcript weight, second component",
            1: "Sample score, second component",
        },
    )

    # present the heat maps
    heatmap(
        pd.read_excel(
            data_dir + "/supplement_dm2_Association_association_map.xlsx", index_col=0
        ),
        False,
        o_map_name=plot_dir + "/hm1-dm2-assoc.html",
    )
    heatmap(
        pd.read_excel(
            data_dir + "/supplement_dm2_C1.DM2-NGT_association_map.xlsx", index_col=0
        ),
        False,
        o_map_name=plot_dir + "/hm2-dm2-c1dm2ngt-assoc.html",
    )
    heatmap(
        pd.read_excel(
            data_dir + "/supplement_ms_association_map_data.xlsx", index_col=0
        ),
        False,
        o_map_name=plot_dir + "/hm3-ms-assoc.html",
    )
    heatmap(
        pd.read_excel(
            data_dir + "/supplement_tcga_association_map_data.xlsx", index_col=0
        ),
        False,
        o_map_name=plot_dir + "/hm4-tcga-assoc.html",
    )

    show_tcga_opg_histogram_plot()
    make_pls_anova_comparison_plot(results_TCGA_status[0],results_TCGA_status[1])

    """ Example execution of draw boxplots
        draw_gene_boxplot ( 'StatusLabel' , convert_file = './naming_and_annotations/biomart/mart_export.txt' ,
                        afile = analyte_df , jfile = journal_df ,
                        ofile = './ms_transcripts_output.png' , bLog2 = False ,
                        genelist = names , grid = [5,4] , color_by = None ,
                        non_adjoint_covariate_groups = None , colors = None )
    """
